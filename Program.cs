﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace EpicAuth
{
    class Program
    {
        static void Main(string[] args)
        {
            if (File.Exists(@"eosgame.txt"))
            {
                try
                {
                    string gameLocation = File.ReadAllText(@"eosgame.txt");
                    var processInfo = new ProcessStartInfo
                    {
                        FileName = gameLocation,
                        Arguments = string.Join(' ', args.Skip(1))
                    };
                    Process.Start(processInfo);
                }
                catch (Exception ex)
                {
                    if (File.Exists(@"eoserr.txt"))
                    {
                        File.Delete(@"eoserr.txt");
                    }
                    using (FileStream fs = File.Create(@"eoserr.txt"))
                    {
                        Byte[] content = new UTF8Encoding(true).GetBytes("An error occurred, make sure the directory is valid. The directory file will open in 2 seconds.\nПеревод от Гугла: Произошла ошибка, убедитесь, что каталог действителен. Файл откроется через 2 секунды.\n \nError (Погрешность):\n \n" + ex);
                        fs.Write(content, 0, content.Length);
                    }

                    Process.Start("notepad.exe", "eoserr.txt");
                    Thread.Sleep(2000);
                    Process.Start("notepad.exe", "eosgame.txt");
                }
            }
            else
            {
                using (FileStream fs = File.Create(@"eosgame.txt"))
                {
                    Byte[] content = new UTF8Encoding(true).GetBytes("Welcome to EpicAuth! Put in the directory to the game here, then run again.\nПеревод от Гугла: Добро пожаловать в EpicAuth! Поместите сюда каталог с игрой, затем снова запустите.");
                    fs.Write(content, 0, content.Length);
                }

                Process.Start("notepad.exe", "eosgame.txt");
            }
        }
    }
}
